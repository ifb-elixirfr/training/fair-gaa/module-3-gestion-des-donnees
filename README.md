# Module 3 - Gestion des données

## Description Cours Magistral (CM)

⏱️ Durée : 2h

📑 Plan :
- Présentation des grands entrepôts de données (ENA SRA Ensembl)
- Gestion des données
- Importance des métadonnées

📚 Supports : 
- [Slides](https://ifb-elixirfr.gitlab.io/training/fair-gaa/module-3-gestion-des-donnees/)

## Description Travaux Pratiques (TP)

⏱️ Durée : 3h

📑 Plan:
- Interrogation et récupération de données depuis le NCBI et l’ENA
  - NCBI datasets -> Genomes
  - ENA -> Reads + métadonnées

Contexte: comparer la qualité des génomes proches de notre espèce d'intérêt avec Quast et BUSCO

📚 Supports : 
- [Tutorial](https://ifb-elixirfr.gitlab.io/training/fair-gaa/module-3-gestion-des-donnees/tutorial.html)
